# Online Exam and Grade Management (Web-Application)
# About : 
- Description: A web application for student to take online test and teacher can give results.
- Role: Student, Teacher
- Role student can take the test and search for mark
- Role teacher can view student exam result list, give mark, search student result 
- Languages programing, libraries and frameworks : C#, ADO .Net Framework, Html, CSS, JavaScript

# Home Screen Layout: 
![Layout] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__17_.PNG)
![Layout] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__18_.PNG)
![Layout] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__19_.PNG)
![Layout] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__20_.PNG)
![Layout] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__21_.PNG)

# My features: 
## 1. Login
#Requirement:
- Login Successful will redirect to the Home page
- When enter the wrong user name or password, the page will display "Wrong user name or password" and user must re-input 
Login Screen Layout: 

![1. Login] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/16.PNG)

## 2. Choose test 
#Requirement:
- Student can choose subjects, level(Easy, Medium, Hard)
- Click TAKE to redirect to test progress page
Choose Test Screen Layout: 

![2. Choose] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__18_.PNG)

## 3. Test Progress
#Requirement:
- Student enter name and do the test 
- When the timer go to 00:00 the web will automatically submit the result to server
Test Progress Screen Layout: 
- Check valid input

![2. Choose] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__22_.PNG)

## 4. Grade list
#Requirement:
- Teacher can filter by subject name or by modes
- Click View will redirect to View Result Page

![4. Grade list] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__23_.PNG)

## 5. Exam Result Page
#Requirement:
- Teacher can see exam result of student and give mark
- After submit the page will redirect to home screen

![5. Exam Result Page] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__24_.PNG)

## 6. View Grade
#Requirement:
- Student can filter list by search their name in textbox

![6. View Grade Page] (https://gitlab.com/kientthe160491/mini-exam/-/raw/main/Screenshot__23_.PNG)

